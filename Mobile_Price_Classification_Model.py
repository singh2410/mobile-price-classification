#!/usr/bin/env python
# coding: utf-8

# # Mobile Price Classification Model
# #By- Aarush Kumar
# #Dated: June 15,2021

# In[1]:


import pandas as pd
import numpy as np
import matplotlib.pyplot as plt


# In[2]:


df_train=pd.read_csv(r'/home/aarush100616/Downloads/Projects/Mobile Price Classification/train.csv')
df_test=pd.read_csv(r'/home/aarush100616/Downloads/Projects/Mobile Price Classification/test.csv')


# In[3]:


df_train


# In[4]:


df_test


# ## Exploratory Data Analysis

# In[5]:


df_train.shape


# In[6]:


df_train.size


# In[7]:


df_test.shape


# In[8]:


df_test.size


# In[9]:


df_train.info()


# In[10]:


df_test.info()


# In[11]:


df_train.isnull().sum()


# In[12]:


df_test.isnull().sum()


# In[13]:


df_train.describe()


# In[14]:


df_test.describe()


# In[15]:


df_train.plot(x='price_range',y='ram',kind='scatter')
plt.show()


# In[16]:


df_train.plot(x='price_range',y='battery_power',kind='scatter')
plt.show()


# In[17]:


df_train.plot(x='price_range',y='fc',kind='scatter')
plt.show()


# In[18]:


df_train.plot(x='price_range',y='n_cores',kind='scatter')
plt.show()


# In[19]:


df_train.plot(kind='box',figsize=(20,10))
plt.show()


# In[20]:


X=df_train.drop('price_range',axis=1)


# In[21]:


X


# In[22]:


df_test=df_test.drop('id',axis=1)


# In[23]:


df_test.head()


# In[24]:


df_test.shape


# In[25]:


Y=df_train['price_range']


# In[26]:


Y


# In[27]:


from sklearn.preprocessing import StandardScaler
std=StandardScaler()


# In[28]:


X_std=std.fit_transform(X)
df_test_std=std.transform(df_test)


# In[29]:


X_std


# In[30]:


df_test_std


# ## Training Model

# ### 1)-Decision Tree

# In[31]:


from sklearn.tree import DecisionTreeClassifier
dt=DecisionTreeClassifier()


# In[32]:


dt.fit(X_std,Y)


# In[33]:


dt.predict(df_test_std)


# In[34]:


df_test


# ### 2)-KNN Algorithm

# In[35]:


from sklearn.neighbors import KNeighborsClassifier
knn=KNeighborsClassifier()


# In[36]:


knn.fit(X_std,Y)


# In[37]:


knn.predict(df_test_std)


# ### 3)-Logistic Regression

# In[38]:


from sklearn.linear_model import LogisticRegression
lr=LogisticRegression()


# In[39]:


lr.fit(X_std,Y)


# In[40]:


lr.predict(df_test_std)


# In[41]:


X


# In[42]:


Y


# ## Splitting data into training & testing

# In[43]:


from sklearn.model_selection import train_test_split


# In[44]:


X_train,X_test,Y_train,Y_test=train_test_split(X,Y,test_size=0.2,random_state=1)


# In[45]:


X_train


# In[46]:


X_test


# In[47]:


Y_train


# In[48]:


Y_test


# ## 1)-Decision Tree

# In[49]:


from sklearn.tree import DecisionTreeClassifier
dt=DecisionTreeClassifier()


# In[50]:


dt.fit(X_train,Y_train)


# In[51]:


Y_pred=dt.predict(X_test)


# In[52]:


Y_pred


# In[53]:


Y_test


# In[54]:


from sklearn.metrics import accuracy_score


# In[55]:


dt_ac=accuracy_score(Y_test,Y_pred)


# In[56]:


dt_ac*100


# ## 2)-Knn

# In[57]:


X_train_std=std.fit_transform(X_train)
X_test_std=std.transform(X_test)
X_test_std


# In[58]:


from sklearn.neighbors import KNeighborsClassifier
knn=KNeighborsClassifier()


# In[59]:


knn.fit(X_train,Y_train)


# In[60]:


Y_pred=knn.predict(X_test_std)


# In[61]:


Y_pred


# In[62]:


Y_test


# In[63]:


knn_ac=accuracy_score(Y_test,Y_pred)


# In[64]:


knn_ac*100


# ## 3)-Logistic Regression

# In[65]:


from sklearn.linear_model import LogisticRegression
lr=LogisticRegression()


# In[66]:


lr.fit(X_train_std,Y_train)


# In[67]:


Y_pred=lr.predict(X_test_std)


# In[68]:


Y_pred


# In[69]:


lr_ac=accuracy_score(Y_test,Y_pred)


# In[70]:


lr_ac*100


# In[71]:


plt.bar(x=['dt','knn','lr'],height=[dt_ac,knn_ac,lr_ac])
plt.xlabel("Algorithms")
plt.ylabel("Accuracy Score")
plt.show()

